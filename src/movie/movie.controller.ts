import {
  BadRequestException,
  Body,
  Controller,
  Get,
  NotFoundException,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import { MovieService } from './movie.service';
import {
  ApiResponse,
  ApiTags,
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiUnauthorizedResponse,
  ApiInternalServerErrorResponse,
  ApiBadRequestResponse,
  ApiNotFoundResponse,
  ApiQuery,
} from '@nestjs/swagger';
import { Rate, Rating } from './shared/rate';
import { Details } from './shared/details';

@ApiTags('Movies')
@Controller('movies')
export class MovieController {
  constructor(private readonly movieService: MovieService) {}

  @ApiBearerAuth()
  @Get('top-rated')
  @ApiOperation({ summary: 'List top rated movies' })
  @ApiResponse({
    status: 200,
    type: [Details],
    description: 'List movies details.',
  })
  @ApiUnauthorizedResponse({ status: 401, description: 'Unauthorized.' })
  @ApiInternalServerErrorResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async listTopRated(@Query('userId') userId: string): Promise<any> {
    try {
      const response = await this.movieService.listTopRated(userId);
      return response;
    } catch (error) {
      return error;
    }
  }

  @ApiBearerAuth()
  @Get('10-popular-in-brasil')
  @ApiOperation({ summary: 'List 10 most popular movies in Brazil' })
  @ApiResponse({
    status: 200,
    type: [Details],
    description: 'List movies details.',
  })
  @ApiUnauthorizedResponse({ status: 401, description: 'Unauthorized.' })
  @ApiInternalServerErrorResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async listMostPopular(@Query('region') region: string): Promise<any> {
    try {
      const response = await this.movieService.listMostPopular(region);
      return response;
    } catch (error) {
      return error;
    }
  }

  @ApiBearerAuth()
  @Get('movie-details/:movieId')
  @ApiOperation({ summary: 'Movie details by movieId' })
  @ApiResponse({
    status: 200,
    type: Details,
    description: 'Movies details.',
  })
  @ApiUnauthorizedResponse({ status: 401, description: 'Unauthorized.' })
  @ApiInternalServerErrorResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async movieDetails(@Param('movieId') movieId: string): Promise<any> {
    try {
      const response = await this.movieService.details(movieId);
      return response;
    } catch (error) {
      return error;
    }
  }

  @ApiBearerAuth()
  @Post(':movieId')
  @ApiOperation({ summary: 'Rate movies' })
  @ApiResponse({
    status: 200,
    type: Rate,
    description: 'Response movie rated.',
  })
  @ApiBadRequestResponse({
    status: 400,
    description: 'Você já curtiu esse filme.',
  })
  @ApiNotFoundResponse({
    status: 404,
    description: 'Filme não encontrado.',
  })
  @ApiUnauthorizedResponse({ status: 401, description: 'Unauthorized.' })
  @ApiInternalServerErrorResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  @ApiParam({
    name: 'movieId',
    type: String,
    description: 'Movie id',
    example: '234',
  })
  @ApiQuery({
    name: 'userId',
    type: String,
    description: 'User id',
    example: '64b347a80d637fbaffa06a8c',
  })
  async rate(
    @Body() body: Rating,
    @Param('movieId') movieId: string,
    @Query('userId') userId: string,
  ): Promise<any> {
    try {
      const { rating } = body;
      const response = await this.movieService.rate(movieId, rating, userId);
      return response;
    } catch (error) {
      if (error.errors?.movieId) {
        throw new BadRequestException('Você já curtiu esse filme');
      } else {
        if (error.status_message) {
          throw new NotFoundException('Filme não encontrado');
        }
        return error;
      }
    }
  }

  @ApiBearerAuth()
  @Get(':movieId')
  @ApiOperation({ summary: 'Get rated by movie id' })
  @ApiParam({
    name: 'movieId',
    type: String,
    description: 'Movie id',
    example: '234',
  })
  @ApiResponse({
    status: 200,
    type: Rate,
    description: 'Response movie rated.',
  })
  @ApiUnauthorizedResponse({ status: 401, description: 'Unauthorized.' })
  @ApiNotFoundResponse({
    status: 404,
    description: 'Filme não encontrado.',
  })
  @ApiInternalServerErrorResponse({
    status: 500,
    description: 'Internal Server Error.',
  })
  async getRatedByMovieId(@Param('movieId') movieId): Promise<any> {
    try {
      const response = await this.movieService.getRatedByMovieId(movieId);
      return response;
    } catch (error) {
      if (error.status_message) {
        throw new NotFoundException('Filme não encontrado');
      }
      return error;
    }
  }
}
