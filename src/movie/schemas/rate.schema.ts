import * as mongoose from 'mongoose';

export const RateSchema = new mongoose.Schema(
  {
    id: mongoose.Schema.Types.ObjectId,
    movieId: String,
    userId: String,
    original_title: String,
    overview: String,
    vote_average: Number,
    poster_path: String,
    rating: Number,
  },
  { versionKey: false },
);
