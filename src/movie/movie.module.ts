import { Module } from '@nestjs/common';
import { MovieService } from './movie.service';
import { MovieController } from './movie.controller';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { RateSchema } from './schemas/rate.schema';
import { UsersModule } from 'src/users/users.module';

@Module({
  imports: [
    MongooseModule.forFeatureAsync([
      {
        name: 'Rate',
        useFactory: () => {
          const schema = RateSchema;
          // eslint-disable-next-line @typescript-eslint/no-var-requires
          schema.plugin(require('mongoose-unique-validator'), {
            message: 'BAD_REQUEST',
          });
          return schema;
        },
      },
    ]),
    HttpModule,
    UsersModule,
  ],
  controllers: [MovieController],
  providers: [MovieService],
})
export class MovieModule {}
