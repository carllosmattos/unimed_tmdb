import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export class Rate extends Document {
  id: string;

  @ApiProperty({ example: '123', description: 'Movie Id' })
  movieId: string;

  @ApiProperty({ example: '64b347a80d637fbaffa06a8c', description: 'Movie Id' })
  userId: string;

  @ApiProperty({
    example: 'As tranças do rei careca',
    description: 'movie name',
  })
  original_title: string;

  @ApiProperty({
    example: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    description: 'movie overview',
  })
  overview: string;

  @ApiProperty({
    example: 9.7,
    description: 'movie vote average',
  })
  vote_average: number;

  @ApiProperty({
    example: '/loren-ipsum.jpg',
    description: 'movie poster path',
  })
  poster_path: string;

  @ApiProperty({ example: 9.9, description: 'Rate movie' })
  rating?: number;
}

export class Rating extends Document {
  @ApiProperty({ example: 10.0, description: 'Rate movie' })
  rating: number;
}
