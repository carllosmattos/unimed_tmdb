import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';

export class Details extends Document {
  @ApiProperty({ example: 123, description: 'Movie Id' })
  id: string;

  @ApiProperty({
    example: ['Ação'],
    description: 'Movie genres',
  })
  genres: [string];

  @ApiProperty({
    example: 'As tranças do rei careca',
    description: 'movie name',
  })
  original_title: string;

  @ApiProperty({
    example: `Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.`,
    description: 'movie overview',
  })
  overview: string;

  @ApiProperty({
    example: '2023-07-18',
    description: 'movie release date',
  })
  release_date: string;

  @ApiProperty({
    example: 150,
    description: 'movie runtime',
  })
  'runtime': number;

  @ApiProperty({
    example: 10.3,
    description: 'movie vote average',
  })
  vote_average: number;

  @ApiProperty({
    example: '/loren-ipsum.jpg',
    description: 'movie poster path',
  })
  poster_path: string;
}
