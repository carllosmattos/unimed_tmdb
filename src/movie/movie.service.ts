import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { catchError, firstValueFrom } from 'rxjs';
import { AxiosError } from 'axios';
import { ConfigService } from '@nestjs/config';
import { Rate } from 'src/movie/shared/rate';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { UsersService } from 'src/users/users.service';
import { User } from 'src/users/shared/user';

@Injectable()
export class MovieService {
  constructor(
    @InjectModel('Rate') private readonly rateModel: Model<Rate>,
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
    private readonly userService: UsersService,
  ) {}

  async listTopRated(userId: string): Promise<any> {
    const data = await this.rateModel
      .find({ userId }, { __v: 0 })
      .sort({ rating: 1 });

    const movies: any = [];
    data.map((movie: any) => {
      return movies.push({
        id: movie.movieId,
        original_title: movie.original_title,
        overview: movie.overview,
        vote_average: movie.vote_average,
        poster_path: movie.poster_path,
        rating: movie.rating,
      });
    });

    return movies;
  }

  async listMostPopular(region: string): Promise<any> {
    const tmdbUrl = this.configService.get<string>('tmdbUrl');
    const apiKey = this.configService.get<string>('apiKey');

    const { data } = await firstValueFrom(
      this.httpService
        .get<any>(
          `${tmdbUrl}/popular?${apiKey}&region=${region}&language=pt-BR`,
        )
        .pipe(
          catchError((error: AxiosError) => {
            throw error.response.data;
          }),
        ),
    );

    const movies: any = [];
    data.results.map((movie: any) => {
      return movies.push({
        id: movie.id,
        original_title: movie.original_title,
        overview: movie.overview,
        vote_average: movie.vote_average,
        poster_path: movie.poster_path,
      });
    });

    const tenPaginationMovies = movies.slice(0, 10);

    return tenPaginationMovies;
  }

  async details(movieId: string): Promise<any> {
    const tmdbUrl = this.configService.get<string>('tmdbUrl');
    const apiKey = this.configService.get<string>('apiKey');

    const { data } = await firstValueFrom(
      this.httpService
        .get<any>(`${tmdbUrl}/${movieId}?${apiKey}&language=pt-BR`)
        .pipe(
          catchError((error: AxiosError) => {
            throw error.response.data;
          }),
        ),
    );

    const genres = [];
    data.genres.map((gender) => {
      genres.push(gender.name);
    });

    return {
      id: data.id,
      genres: genres,
      original_title: data.original_title,
      overview: data.overview,
      release_date: data.release_date,
      runtime: data.runtime,
      popularity: data.popularity,
      vote_average: data.vote_average,
      poster_path: data.poster_path,
    };
  }

  async rate(movieId: string, rating: number, userId: string): Promise<any> {
    const details = await this.details(movieId);

    const createRate = {
      movieId,
      userId,
      original_title: details.original_title,
      overview: details.overview,
      vote_average: details.vote_average,
      poster_path: details.poster_path,
      rating,
    };

    const rate = new this.rateModel(createRate);

    const getRatedByMovieId = await this.getRatedByMovieIdAndUserId(
      movieId,
      userId,
    );

    if (getRatedByMovieId) {
      const user = await this.userService.findUserById(userId);
      const informedUser = user.toObject();

      if (informedUser._id.toString() === getRatedByMovieId.userId) {
        await this.rateModel.updateOne({ userId }, { rating });
      }
    } else {
      rate.save();
    }

    return createRate;
  }

  async getRatedByMovieId(movieId: string): Promise<any> {
    return await this.rateModel.findOne({ movieId }, { __v: 0 });
  }

  async getRatedByMovieIdAndUserId(
    movieId: string,
    userId: string,
  ): Promise<any> {
    return await this.rateModel.findOne({ movieId, userId }, { __v: 0 });
  }
}
