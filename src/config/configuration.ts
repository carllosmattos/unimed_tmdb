export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  database: {
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
  },
  mongoUri: process.env.MONGO_URI,
  secret: process.env.SECRET,
  expiresIn: process.env.EXPIRESIN,
  tmdbUrl: process.env.TMDB_URL,
  apiKey: process.env.API_KEY,
});
