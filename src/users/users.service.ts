import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from './shared/user';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  async getAllUsers() {
    return await this.userModel.find().exec();
  }

  async createUser(user: User) {
    const hash = await bcrypt.hash(user.password, 10);
    const createUser = new this.userModel({ ...user, password: hash });

    return await createUser.save();
  }

  async findUserByEmail(email: string): Promise<User | undefined> {
    return await this.userModel.findOne({ email }, { __v: 0 });
  }

  async findUserById(id: string): Promise<User | undefined> {
    return await this.userModel.findOne({ _id: id }, { __v: 0 }).exec();
  }
}
