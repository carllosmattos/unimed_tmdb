import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema(
  {
    id: mongoose.Schema.Types.ObjectId,
    name: String,
    email: {
      type: String,
      unique: true,
    },
    password: String,
  },
  { versionKey: false },
);
