import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class User extends Document {
  @ApiProperty({ example: '64b14143e6122329765caaa6', description: 'user id' })
  id: string;

  @ApiProperty({ example: 'Joe doe', description: 'user name' })
  name: string;

  @ApiProperty({ example: 'user@email.com', description: 'user email' })
  email: string;

  @ApiProperty({ example: 'hash string', description: 'user email' })
  password: string;
}

export class CreateUser extends Document {
  @ApiProperty({ example: '64b14143e6122329765caaa6', description: 'user id' })
  id: string;

  @ApiProperty({ example: 'Joe doe', description: 'user name' })
  name: string;

  @ApiProperty({ example: 'user@email.com', description: 'user email' })
  email: string;
}
