import {
  Controller,
  Get,
  Post,
  Body,
  Param,
  BadRequestException,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUser, User } from './shared/user';
import { Public } from 'src/util/auth';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiParam,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';

@ApiTags('Users')
@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @ApiBearerAuth()
  @Get()
  @ApiOperation({ summary: 'List users' })
  @ApiResponse({
    status: 200,
    type: [CreateUser],
    description: 'Response list users.',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  async getAllUsers(): Promise<User[]> {
    try {
      const users = [];
      const response = await this.usersService.getAllUsers();
      response.filter((user) => {
        const objectUser = user.toObject();
        delete objectUser.password;
        users.push(objectUser);
      });
      return users;
    } catch (error) {
      return error;
    }
  }

  @Public()
  @Post()
  @ApiOperation({ summary: 'Create user' })
  @ApiResponse({
    status: 201,
    type: CreateUser,
    description: 'Response user created.',
  })
  @ApiResponse({
    status: 400,
    description: 'O email informado já foi registrado.',
  })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  async createUser(@Body() payload: User): Promise<CreateUser> {
    try {
      const user = await this.usersService.createUser(payload);
      const createUser = user.toObject();
      delete createUser.password;
      return createUser;
    } catch (error) {
      if (error.errors.email) {
        throw new BadRequestException('O email informado já foi registrado');
      } else {
        return error;
      }
    }
  }

  @ApiBearerAuth()
  @Get(':email')
  @ApiOperation({ summary: 'Get user by email' })
  @ApiResponse({
    status: 200,
    type: User,
    description: 'Response user by email.',
  })
  @ApiParam({
    name: 'email',
    type: String,
    description: 'User email',
    example: 'a@a.com',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  async findUserByEmail(@Param('email') email: string): Promise<User> {
    try {
      return await this.usersService.findUserByEmail(email);
    } catch (error) {
      return error;
    }
  }
}
