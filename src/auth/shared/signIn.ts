import { Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export class SignIn extends Document {
  @ApiProperty({ example: 'user@email.com', description: 'user email' })
  email: string;

  @ApiProperty({ example: '123456', description: 'user email' })
  password: string;
}
