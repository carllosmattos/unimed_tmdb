import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(
    private usersService: UsersService,
    private jwtService: JwtService,
  ) {}

  async signIn(email: string, password: string): Promise<any> {
    try {
      const user = await this.usersService.findUserByEmail(email);
      if (!user) {
        throw new BadRequestException('Email não encontrado');
      }

      const isMatch = await bcrypt.compare(password, user?.password);
      if (!isMatch) {
        throw new UnauthorizedException('Senha incoreta');
      }
      const payload = { sub: user._id, email: user.email, name: user.name };

      return {
        access_token: await this.jwtService.signAsync(payload),
      };
    } catch (error) {
      return error;
    }
  }
}
