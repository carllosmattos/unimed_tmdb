import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Post,
  Request,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { Public } from 'src/util/auth';
import {
  ApiTags,
  ApiOperation,
  ApiResponse,
  ApiBearerAuth,
} from '@nestjs/swagger';
import { SignIn } from './shared/signIn';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Public()
  @HttpCode(HttpStatus.OK)
  @Post('login')
  @ApiOperation({ summary: 'signIn' })
  @ApiResponse({
    status: 200,
    type: String,
    description: 'Response token JWT.',
  })
  @ApiResponse({
    status: 400,
    description: 'Email não encontrado.',
  })
  @ApiResponse({ status: 401, description: 'Senha incoreta.' })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  async signIn(@Body() signInDto: SignIn): Promise<string> {
    try {
      return await this.authService.signIn(signInDto.email, signInDto.password);
    } catch (error) {
      return error;
    }
  }

  @ApiBearerAuth()
  @Get('profile')
  @ApiOperation({ summary: 'Profile user' })
  @ApiResponse({
    status: 200,
    type: Object,
    description: 'Response profile user.',
  })
  @ApiResponse({ status: 401, description: 'Unauthorized.' })
  @ApiResponse({ status: 500, description: 'Internal Server Error.' })
  getProfile(@Request() req) {
    return req.user;
  }
}
