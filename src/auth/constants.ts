import { ConfigService } from '@nestjs/config';

export const jwtConstants = () => {
  const configService = new ConfigService();
  const secret = configService.get<string>('secret');
  const expiresIn = configService.get<number>('expiresIn');

  return {
    secret,
    expiresIn,
  };
};
